/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Inspi
 */
public class SecondServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SecondServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SecondServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<font size=\"4\" face=\"Arial\" color=\"#888\">");
        String name = request.getParameter("name");
        String pass = request.getParameter("password");
        String type=request.getParameter("type");
        int id1=0;
        String id="null";
        
        try {
           Class.forName("com.mysql.jdbc.Driver");            
         Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ritika_db","root","student");
         PreparedStatement ps ;
         if(type.equals("witness"))
           ps=con.prepareStatement("select * from witness where username=? and password=?");   
         else    
         ps=con.prepareStatement("select * from victim where username=? and password=?");
         
         ps.setString(1, name);
         ps.setString(2, pass);
         
         ResultSet rs =ps.executeQuery();
         rs.next();
         id1=rs.getInt("id");
         id=Integer.toString(id1);
         
            System.out.println("secondservlet:"+id1);
         
        } catch (Exception e) 
        {
            System.out.println(e);
        }

        
        if(Validate.checkuser(name,pass,type))
        {
            
             HttpSession session = request.getSession();
             session.setAttribute("id",id);   
             session.setAttribute("type",type);   
             
            RequestDispatcher rs = request.getRequestDispatcher("welcome.html"); 
            rs.forward(request, response);
        }
        else
        {        
           out.println("Username or Password incorrect "+type);
           RequestDispatcher rs = request.getRequestDispatcher("login.html");
           rs.include(request, response);
        }


    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
