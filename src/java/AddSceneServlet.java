/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.Naming;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Inspi
 */
public class AddSceneServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSceneServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSceneServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  
    {
       response.setContentType("text/html;charset=UTF-8");
       PrintWriter out = response.getWriter();
       
       Boolean b1=false,b2=false;
       
       String city=request.getParameter("q1");
       String locality=request.getParameter("q2");
       String date=request.getParameter("q3");
       String tshirtcolor=request.getParameter("q4");
       String pantscolor=request.getParameter("q5");
       String weapon=request.getParameter("q6");
       String type=request.getParameter("q7");
       
       
       HttpSession session = request.getSession();
       String id=(String) session.getAttribute("id");
       String cat=(String)session.getAttribute("type");
       
        try
       {
          String ls_url = "rmi://127.0.0.1/addServer";
          AddServletIntf lsi = (AddServletIntf)Naming.lookup(ls_url);
          b1 = lsi.add1(city,locality,date,cat,id);
          b2 = lsi.add2(tshirtcolor,pantscolor,weapon,type,cat,id);
           
       }
        catch(Exception e)
       {
           System.out.println(e);
       }
        RequestDispatcher rd;
        out.println("<font size=\"4\" face=\"Arial\" color=\"#888\">");
        
        if(b1&&b2)
        {
             rd = request.getRequestDispatcher("success.html");
             rd.forward(request, response);
        }
        else
        {
            out.println("Failure in entering details");
            rd = request.getRequestDispatcher("addscene.html");
             rd.forward(request, response);
        }
       
        
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
