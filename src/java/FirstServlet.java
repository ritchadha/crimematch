/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Inspi
 */
public class FirstServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FirstServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FirstServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
       
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String name=request.getParameter("field1");
        String address=request.getParameter("field2");
        String email=request.getParameter("field3");
        String phone=request.getParameter("field4");
        String pass=request.getParameter("field5");
        String conpass=request.getParameter("field6");
       
        int flag1=0,flag2=0,flag3=0; 
      Pattern pattern = Pattern.compile("\\d{2}-\\d{10}");
      Matcher matcher = pattern.matcher(phone);
    
      out.println("<font size=\"4\" face=\"Arial\" color=\"#888\">");
    
      if (matcher.matches())
      {
          
    	  flag1=1;
      }
      else 
      {    
           out.println("Phone number entered is incorrect.");
         RequestDispatcher rs = request.getRequestDispatcher("index.html");
         rs.include(request, response); 
          
      }
      if(pass.equals(conpass))
      {
          
          flag2=1;
      }
     else
          {
           out.println("Passwords do not match.Enter again.");
          RequestDispatcher rs = request.getRequestDispatcher("index.html");
          rs.include(request, response); 
          }
      if(request.getParameter("field7")!=null)
      {
          flag3=1;
      }
      else
      { 
          out.println("You do not agree to our terms and policies. ");
         RequestDispatcher rs = request.getRequestDispatcher("index.html");
        rs.include(request, response);
      }
     if((flag1==1)&(flag2==1)&(flag3==1))
         
        {
           try {
               Class.forName("com.mysql.jdbc.Driver");
                Connection  con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ritika_db","root","student");
                PreparedStatement ps;
                
                if(request.getParameter("type").equals("witness"))
                ps=con.prepareStatement("insert into witness(username,address,password,email,phone)values(?,?,?,?,?)");
                
                else
                ps=con.prepareStatement("insert into victim(username,address,password,email,phone)values(?,?,?,?,?)");
                
                ps.setString(1, name);
                ps.setString(2, address);
                ps.setString(3, pass);
                ps.setString(4, email);
                ps.setString(5, phone);
                int i=ps.executeUpdate();
        
        if(i>0)
        {
        out.println("Thank you for registering with us as the "+request.getParameter("type")+"!");
        RequestDispatcher rs = request.getRequestDispatcher("login.html");
        rs.include(request, response);
        }

           } catch (Exception e) {
               e.printStackTrace(); 
           }
         

        }

      }
    
       
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
