
import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

public class AddServletImpl extends UnicastRemoteObject implements AddServletIntf{
    
    public AddServletImpl() throws RemoteException
    {
        
    }
   
   public boolean add1(String city,String locality,String date,String cat,String id)throws RemoteException
    {
        Connection conn;
        String sql;
        Statement stmt;
        ResultSet rs;
        Boolean b=false;
        
     try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection  con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ritika_db","root","student");
                PreparedStatement ps;
                
                if(cat.equals("witness"))
                
                ps=con.prepareStatement("insert into scene(city,locality,date_time,witness_id)values(?,?,?,?)");
                else
                
                ps=con.prepareStatement("insert into scene(city,locality,date_time,victim_id)values(?,?,?,?)");   
                    
                    
                ps.setString(1, city);
                ps.setString(2, locality);
                ps.setString(3, date);
                ps.setString(4,id);
               
                int i=ps.executeUpdate();
                System.out.println("exc");
        if(i>0)
        {
            b=true;
        }

           } catch (Exception e) {
               e.printStackTrace(); 
           }
     return b;
    }
    
   public boolean add2(String tshirtcolor,String pantscolor,String weapon,String type,String cat,String id)throws RemoteException
   {
        Connection conn;
        String sql;
        Statement stmt;
        ResultSet rs;
        Boolean b=false;
        
     try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection  con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ritika_db","root","student");
                PreparedStatement ps;
                
                if(cat.equals("witness"))
                ps=con.prepareStatement("insert into vic_desc(tshirt,pant,weapon,type,witness_id)values(?,?,?,?,?)");
                
                else
                 ps=con.prepareStatement("insert into vic_desc(tshirt,pant,weapon,type,victim_id)values(?,?,?,?,?)");
                System.out.println(id);
                ps.setString(1, tshirtcolor);
                ps.setString(2, pantscolor);
                ps.setString(3, weapon);
                ps.setString(4,type);
                ps.setString(5,id);

               
                int i=ps.executeUpdate();
        
        if(i>0)
        {
            b=true;
        }

           } catch (Exception e) {
               e.printStackTrace(); 
           }
     return b;
    }
  
   
}
